import VueRouter from 'vue-router';

var routes = [
    { path: '*', redirect: '/' },
    {
        path: '/',
        component: require('./components/Dashboard'),
        meta: {

        }
    },
    {
        path: '/training/:p/:q',
        component: require('./components/Training'),
        meta: {

        }
    },
    // {
    //     path: '/broker-training',
    //     component: require('./components/BrokerTraining'),
    //     children: [
    //         {path: '/broker-1.1'},
    //         {path: '/broker-1.2'},
    //         {path: '/broker-1.3'},
    //         {path: '/broker-1.4'},
    //         {path: '/broker-2.1'},
    //         {path: '/broker-2.2'},
    //         {path: '/broker-2.3'},
    //         {path: '/broker-2.4'},
    //         {path: '/broker-3.1'},
    //         {path: '/broker-3.2'},
    //         {path: '/broker-3.3'},
    //         {path: '/broker-3.4'},
    //         {path: '/broker-4.1'},
    //         {path: '/broker-4.2'},
    //         {path: '/broker-4.3'},
    //         {path: '/broker-4.4'},
    //     ],
    //     meta: {
    //
    //     }
    // },
    {
        path: '/tools/:p',
        component: require('./components/Tools'),
        meta: {

        }
    },
    {
        path: '/branding/:p',
        component: require('./components/Branding'),
        meta: {

        }
    },
    {
        path: '/scripts/:p',
        component: require('./components/scripts'),
        meta: {

        }
    },

];

export default new VueRouter({
    routes: routes
});